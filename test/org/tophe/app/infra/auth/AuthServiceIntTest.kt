package org.tophe.app.infra.auth

import io.ktor.application.*
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.assertj.core.api.Assertions.assertThat
import org.tophe.app.main
import kotlin.test.Test

class AuthServiceIntTest {

    @Test
    fun `returns bad request if auth code is missing`() {
        withTestApplication(Application::main) {

            with(handleRequest(HttpMethod.Get, "/user-details")) {
                assertThat(response.status()).isEqualTo(HttpStatusCode.BadRequest)
                assertThat(response.content).isEqualTo("authorization_code is missing")
            }

        }
    }

    @Test
    fun `returns user details for provided auth code`() {
        withTestApplication(Application::main) {
            val authCode = "xxx"
            val call = handleRequest(HttpMethod.Get, "/user-details") {
                addHeader("AUTHORIZATION_CODE", authCode)
            }
            with(call) {
                assertThat(response.status()).isEqualTo(HttpStatusCode.OK)
                assertThat(response.content).isEqualTo("{\"userId\":\"12345\",\"userName\":\"jo\"}")
            }

        }
    }
}
