package org.tophe.app

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.CallLogging
import io.ktor.features.ConditionalHeaders
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.error
import org.slf4j.event.Level
import org.tophe.app.infra.auth.AuthService
import org.tophe.app.infra.auth.OrangeAuthConfig.Companion.loadOrangeAuthConfig
import org.tophe.app.infra.session.MySession
import org.tophe.app.infra.session.MySession.Companion.installSessions
import org.tophe.app.infra.session.MySession.Companion.sessionCheck


@KtorExperimentalAPI
fun Application.main() {
    val authorisationCodePath = "/authorization-code"
    val authService = AuthService(log, loadOrangeAuthConfig(environment.config))

    install(ConditionalHeaders)
    install(CallLogging) { level = Level.INFO }
    install(ContentNegotiation) { jackson {} }
    installSessions(this)

    routing {

        sessionCheck(this, authorisationCodePath, log, authService)

        get("/") {
            val session = call.sessions.get<MySession>()!!
            call.respondText("Hi session token: ${session.authToken}")
            return@get
        }

        get(authorisationCodePath) {
            val code =
                call.parameters["code"] ?: return@get call.respond(
                    HttpStatusCode.BadRequest,
                    "authorization_code is missing"
                )
            try {
                authService.retrieveUserDetailsWithCode(code).apply {
                    call.sessions.set(MySession(userId, userName, access_token))
                }
                call.respondRedirect("/")
            } catch (e: Exception) {
                log.error(e)
            }
        }

        get("/user-details") {
            val session = call.sessions.get<MySession>()!!
            call.respondText("Your are IN: ${session.authToken}")
            return@get
        }
    }
}

