package org.tophe.app.infra.session

import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.request.path
import io.ktor.response.respondRedirect
import io.ktor.routing.Routing
import io.ktor.sessions.*
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.hex
import org.slf4j.Logger
import org.tophe.app.infra.auth.AuthService

data class MySession(val userId: String, val userName: String, val authToken: String) {
    companion object {

        fun sessionCheck(
            routing: Routing,
            authorisationCodePath: String,
            log: Logger,
            authService: AuthService
        ) {
            routing.intercept(ApplicationCallPipeline.Features) {
                if (call.request.path() == authorisationCodePath) {
                    log.info("[Session] skip session check for path '$authorisationCodePath'")
                    return@intercept
                }

                val session = call.sessions.get<MySession>()
                if (session == null) {
                    log.info("[Session] No session provided in request, redirect to login page")
                    call.respondRedirect(authService.loginUrl())
                    return@intercept finish()
                }
                val userDetails = authService.retrieveUserDetailsWithToken(session.authToken)
                if (userDetails == null) {
                    log.info("[Session] Failed to retrieve user details with (expired?) access token, redirect to login page")
                    call.respondRedirect(authService.loginUrl())
                    return@intercept finish()
                }
                log.info("[Session][${call.request.path()}] found valid sessions for '${userDetails.userName}'")
            }
        }

        @KtorExperimentalAPI
        fun installSessions(app: Application) {
            app.install(Sessions) {
                cookie<MySession>("my_cookie") {
                    val secretSignKey = app.environment.config.property("ktor.deployment.session_sign_key").getString()
                    transform(SessionTransportTransformerMessageAuthentication(hex(secretSignKey)))
                }
            }
        }
    }
}