package org.tophe.app.infra.auth

import io.ktor.client.features.BadResponseStatusException
import io.ktor.client.request.forms.FormDataContent
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.Parameters
import org.slf4j.Logger
import org.tophe.app.infra.httpclient.ReqClient.httpClient

data class AuthService(private val log: Logger, private val config: OrangeAuthConfig) {

    private data class OrangeTokensDTO(
        val token_type: String,
        val access_token: String,
        val expires_in: Int,
        val refresh_token: String,
        val id_token: String
    )

    private data class OrangeUserInfoDTO(val name: String, val sub: String)

    suspend fun retrieveUserDetailsWithCode(code: String): UserDetails {
        val orangeTokensDTO = retrieveTokens(code)
        return retrieveUserDetailsWithToken(orangeTokensDTO.access_token)!!
    }

    private suspend fun retrieveTokens(code: String): OrangeTokensDTO {
        return httpClient().post {
            url(config.tokenUri)
            headers.append("Authorization", "Basic ${config.authorizationHeader}")
            body = FormDataContent(Parameters.build {
                append("grant_type", "authorization_code")
                append("code", code)
                append("redirect_uri", config.redirectUri)
            })
        }
    }

    suspend fun retrieveUserDetailsWithToken(accessToken: String): UserDetails? {
        try {
            val orangeUserInfoDTO = httpClient().get<OrangeUserInfoDTO> {
                url(config.userInfoUri)
                headers.append("Authorization", "Bearer $accessToken")
            }
            orangeUserInfoDTO.apply {
                return UserDetails(sub, name, accessToken)
            }
        } catch (e: BadResponseStatusException) {
            log.info("Failed to retrieve user info with access token '$accessToken'", e)
            return null
        }
    }

    fun loginUrl(): String {
        return "${config.authorizeUri}?response_type=code&client_id=${config.clientId}&scope=${config.scope}&redirect_uri=${config.redirectUri}&state=dat"
    }
}
