package org.tophe.app.infra.auth

import io.ktor.config.ApplicationConfig
import io.ktor.util.KtorExperimentalAPI

data class OrangeAuthConfig(
    val authorizeUri: String,
    val tokenUri: String,
    val userInfoUri: String,
    val authorizationHeader: String,
    val clientId: String,
    val redirectUri: String,
    val scope: String
) {
    companion object {
        @KtorExperimentalAPI
        fun loadOrangeAuthConfig(cfg: ApplicationConfig): OrangeAuthConfig {
            return OrangeAuthConfig(
                cfg.property("ktor.deployment.auth.authorize_uri").getString(),
                cfg.property("ktor.deployment.auth.token_uri").getString(),
                cfg.property("ktor.deployment.auth.userInfo_uri").getString(),
                cfg.property("ktor.deployment.auth.authorization_header").getString(),
                cfg.property("ktor.deployment.auth.client_id").getString(),
                cfg.property("ktor.deployment.auth.redirect_uri").getString(),
                cfg.property("ktor.deployment.auth.scope").getString()
            )
        }
    }
}