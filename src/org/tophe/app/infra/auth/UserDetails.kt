package org.tophe.app.infra.auth

data class UserDetails(val userId: String, val userName: String, val access_token: String)
